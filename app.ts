class Delegate{
    delegateName: string;
    age: number;
    gender: EGenders;
    honesty: number;
    minBribe: number;

    constructor(delegateName, age, gender, honesty,minBribe ){
        this.delegateName = delegateName;
        this.age = age;
        this.gender = gender;
        this.honesty = honesty;
        this.minBribe = minBribe;
    }

} 

class Consignment{
    private name: string;
    mainMember: string;
    members: object[];


  constructor(name: string, mainMember: string, members: object[]) {
    this.name = name
    this.mainMember = mainMember
    this.members = members
  }

}

class Parlament{
    consignments: object[];
    countsOfSits: number;


  constructor(consignments: object[], countsOfSits: number) {
    this.consignments = consignments
    this.countsOfSits = countsOfSits
  }

}


enum EGenders {
    MALE = 'male',
    FEMALE = 'female'
}

let vitaly = new Delegate('Vitaly', 26, EGenders.MALE, 50, 1500)

console.log(vitaly);
